# Making a full bundle
Simply concatenate the certificate together with the following issuer, in the respective order to which the verify the certificate

If this is not possible, then follow this procedure to generate certificate bundles:
https://stackoverflow.com/questions/20101051/how-to-extract-issuer-certificate-from-other-certificate
Do this recursively with issuer to issuer and then simply concatenate the certificates together

## Install certificates on Linux

```
cd ems_certs
./install.sh
```
The result should containe 
```
Updating certificates in /etc/ssl/certs...
5 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
```

## Python virtualenv
When using python `virtualenv`'s and trying to send https requests with a library depending on certifi (requests, etc), you will like run into an error like
```
ssl.SSLCertVerificationError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1108)
```
To address this, you need to add the VestasIssuing cert to the certifi installation in the virtualenv:

```
openssl x509 -in /path/to/VestasIssuingCA02G1.cer  -text >> /path/to/venv/lib/python3.8/site-packages/certifi/cacert.pem
```